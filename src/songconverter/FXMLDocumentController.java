/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package songconverter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author RonCYJ
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField songTitle;
    @FXML
    private TextField songAuthor;
    @FXML
    private TextArea lyricsTextArea;
    @FXML
    private TextArea convertTextArea;
    @FXML
    private Button resetBtn;
    @FXML
    private Button convertBtn;
    @FXML
    private CheckBox pinyinOption;
    @FXML
    private CheckBox pinyinSound;
    @FXML
    private CheckBox pinyinFlag;
    @FXML
    private CheckBox formatFlag;
    @FXML
    private Slider fontSizeSlider;
    @FXML
    private MenuButton menuButton;
    @FXML
    private MenuItem menuItem1;
    @FXML
    private MenuItem menuItem2;
    private final char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    private String title;
    private String author;
    private String verseOrder;
    BufferedInputStream isr;
    BufferedReader br;
    StringBuilder convertedLyrics = new StringBuilder();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    Calendar cal = Calendar.getInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void resetAction(ActionEvent event) throws IOException {
        songTitle.setText("");
        songAuthor.setText("");
        lyricsTextArea.setText("");
        convertTextArea.setText("");
        title = "";
        author = "";
        verseOrder = "";
        convertedLyrics = new StringBuilder();
    }

    @FXML
    private void convertAction(ActionEvent event) throws IOException {
        if (checkInput()|| editallFormatFlag()|| !formatFlag.isSelected()) {
            convertedLyrics = new StringBuilder();
            isr = new BufferedInputStream(new ByteArrayInputStream(lyricsTextArea.getText().getBytes()));
            br = new BufferedReader(new InputStreamReader(isr));
            convertLyrics();
            convertTextArea.setText(convertedLyrics.toString());
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Title is Empty!");
            alert.showAndWait();
        }
    }

    private boolean checkInput() {
        if (!songTitle.getText().isEmpty() && !lyricsTextArea.getText().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void convertLyrics() throws IOException {
        title = songTitle.getText();
        if (!songAuthor.getText().isEmpty()) {
            author = songAuthor.getText();
        } else {
            author = "Author Unknown";
        }
        if (xmlFormatFlag()) {
            appendText("<?xml version='1.0' encoding='UTF-8'?>\n"
                    + "<song xmlns=\"http://openlyrics.info/namespace/2009/song\" version=\"0.8\" createdIn=\"OpenLP 2.2.1\" modifiedIn=\"OpenLP 2.2.1\" modifiedDate=\""
                    + dateFormat.format(cal.getTime())
                    + "T"
                    + timeFormat.format(cal.getTime())
                    + "\">\n"
                    + "  <properties>\n"
                    + "    <titles>\n"
                    + "      <title>" + title + "</title>\n"
                    + "    </titles>\n"
                    + "    <authors>\n"
                    + "      <author>" + author + "</author>\n"
                    + "    </authors>\n"
                    + "  </properties>\n");
            //Start convert Lyrics
            appendText("  <lyrics>\n");
        }
        String line;
        String pinyinText = "";
        String verseName = "NoVerseName";
        String verseCount = "NoVerseName";
        String oldVerseCount = "NoVerseName2";
        int count = 0;
        boolean flag = false;
        while ((line = br.readLine()) != null) {
            pinyinText = "";
            if ((line.length() > 2) && line.substring(0, 3).equals("!!!")) {
                if (xmlFormatFlag()) {
                    verseName = line.substring(3, 5);
                    count = 0;
                    flag = true;
                    verseCount = verseName + alphabet[count];
//                appendText("<verse name=\"" + verseCount + "\">\n"
//                        + "      <lines>");
//                count++;
                }
                if (editallFormatFlag()) {
                    verseName = "---["+line.substring(3)+"]---";
                    appendText(verseName+"\n");
                }
            } else if (line.equals("")) {
                verseCount = verseName + alphabet[count];
                if (flag && xmlFormatFlag()) {
                    appendText("</lines>\n"
                            + "    </verse>\n");
                }
                else if (editallFormatFlag()){
                    appendText(verseName+"\n");
                }
                System.out.println("Break!!");
            } else {
                if (xmlFormatFlag()) {
                        if (verseCount.equalsIgnoreCase(oldVerseCount)) {
                            appendText("<br/>");
                        } else {
                            oldVerseCount = verseCount;
                            appendText("<verse name=\"" + verseCount + "\">\n"
                                    + "      <lines>");
                            count++;
                        }
                        if (count == 26) {
                            verseName = verseCount;
                            count = 0;
                        }
                }
                boolean han = containsHanScript(line);
                System.out.println(han);
                if (han && pinyinFlag.isSelected()) {
                    for (int i = 0; i < line.length(); i++) {
                        if (Integer.toHexString(line.charAt(i)).length() == 4) {
                            // Generate Han word Hex
                            System.out.printf(Integer.toHexString(line.charAt(i)) + "  ");
//                            String[] pinyin = ChineseToPinyinResource.getInstance().getHanyuPinyinStringArray(line.charAt(i));
                            String[] pinyinArray = ChineseToPinyinResource.getInstance().getHanyuPinyinStringArray(line.charAt(i));
                            int pinyinCount = 0;
                            if (!pinyinSound.isSelected()) {
                                for (String temp : pinyinArray) {
                                    pinyinArray[pinyinCount] = temp.substring(0, temp.length() - 1);
                                    pinyinCount++;
                                }
                            }
                            Set<String> pinyin = new HashSet<String>(Arrays.asList(pinyinArray));
//                            System.out.println(pinyin.size());

                            //Space before printing Pinyin
                            if ((pinyinText.length() > 1) && (pinyinText.charAt(pinyinText.length() - 1) != ' ')) {
                                pinyinText = pinyinText + " ";
                            }
                            if (pinyinOption.isSelected()) {
                                for (String text : pinyin) {
                                    pinyinText = pinyinText + text + (pinyin.size() > 1 ? "/" : "");
                                }
                            } else {
                                pinyinText = pinyinText + pinyinArray[0];
                            }
                            pinyinText = pinyinText + " ";

                        } else {
                            pinyinText = pinyinText + line.charAt(i);
                        }
                    }
                    System.out.println("Final: " + pinyinText);
                    appendText(line);
                    if (xmlFormatFlag()) {
                        appendText("<br/>");
                    } else {
                        appendText("\n");
                    }
                    appendText(pinyinText);
                    continue;
                }
                System.out.println("");
                //Print Lyrics
                appendText(line);
                if (!xmlFormatFlag()) {
                    appendText("\n");
                }
            }
        }
        if (xmlFormatFlag()) {
            appendText("</lines>\n"
                    + "    </verse>\n"
                    + "  </lyrics>\n"
                    + "</song>");
        }
    }

    private void appendText(String text) {
        convertedLyrics.append(text);
    }

    // ContainsHanScript Function
    // Source from http://stackoverflow.com/questions/26357938/detect-chinese-character-in-java
    public static boolean containsHanScript(String s) {
//        for (int i = 0; i < s.length();) {
//            int codepoint = s.codePointAt(i);
//            i += Character.charCount(codepoint);
//            if (Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN) {
//                return true;
//            }
//        }
//        return false;

        return s.codePoints().anyMatch(
                codepoint
                -> Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN);
    }

    public boolean xmlFormatFlag() {
        return formatFlag.isSelected() && menuButton.getText().equalsIgnoreCase("XML");
    }

    public boolean editallFormatFlag() {
        return formatFlag.isSelected() && (menuButton.getText().equalsIgnoreCase(menuItem2.getText()) || menuButton.getText().equalsIgnoreCase("Format Type"));
    }

    public void menuItem1OnAction() {
        menuButton.setText(menuItem1.getText());
    }

    public void menuItem2OnAction() {
        menuButton.setText(menuItem2.getText());
    }
}
