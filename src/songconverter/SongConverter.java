/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package songconverter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author RonCYJ
 */
public class SongConverter extends Application {
    String NAME_VERSION = "2";
    String NAME_FEATURE = "2";
    String NAME_BUGFIX = "3";

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));        
        Scene scene = new Scene(root);
        stage.setTitle("Song Converter to OpenLP v"+NAME_VERSION+"."+NAME_FEATURE+"."+NAME_BUGFIX);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        // Set UTF-8 Encoding (http://stackoverflow.com/questions/361975/setting-the-default-java-character-encoding)
        System.setProperty("file.encoding", "UTF-8");
        Field charset = Charset.class.getDeclaredField("defaultCharset");
        charset.setAccessible(true);
        charset.set(null, null);
        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream("LogOutput.txt"), false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SongConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        java.util.Date date = new java.util.Date();
        out.println("------------"+date+"------------");
        System.setOut(out);
        launch(args);
        out.flush();
        out.close();
    }

}
